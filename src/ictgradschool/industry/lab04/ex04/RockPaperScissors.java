package ictgradschool.industry.lab04.ex04;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    private int printPromptGetIntResponse(String prompt_msg) {
        System.out.println(prompt_msg);
        String input_int = Keyboard.readInput();
        return Integer.parseInt(input_int);
    }

    private String printPromptGetStringResponse(String prompt_msg) {
        System.out.println(prompt_msg);
        String input_str = Keyboard.readInput();
        return input_str;
    }

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        // as detailed below
        /**
         * Printing the prompt and reading the input from the user for the name
         */
        String[] names = {"Vita", "Sean", "Andrew", "James", "Dominic"};
        System.out.println(names[names.length]);
        String userName = "";
        userName = printPromptGetStringResponse("Enter name: ");
        System.out.println("1. Rock");
        System.out.println("2. Scissors");
        System.out.println("3. Paper");
        System.out.println("4. Quit");
        int choice = 0;
        choice = printPromptGetIntResponse("Enter choice: ");
        String response = displayPlayerChoice(userName,choice);
        int computerChoice = ROCK;




        /**
         * Printing the prompt and reading the input from the user for the choice of rock, scissors, paper, or quit
         * While the user is playing, display the player choice, determine who wins, and display the results
         * Printing a message to display the goodbye message when the user chooses to quit the application
         */
    }


    public String displayPlayerChoice(String userName, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        String playerChoice = "";

        if (choice == 4) {
            System.out.println("Goodbye");
            // break
                   }
        else if (choice == 1) {
            playerChoice = "Rock";
        }
        else if (choice == 2) {
            playerChoice = "Scissors";
        }
        else if
                (choice == 3) {
            playerChoice = "Paper";
        }
        System.out.println(userName + " chose: " + playerChoice);
        return userName;
    }

    public boolean userWins(int playerChoice, int computerChoice) {
       // playerChoice = choice;
        // TODO Determine who wins and return true if the player won, false otherwise.


        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        return null;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
