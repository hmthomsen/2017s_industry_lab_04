package ictgradschool.industry.lab04.ex03;

/**
 * A guessing game!
 * Use the following pseudocode to write the code:
 * Generate a random number between 1 and 100 and store in a variable named goal
 * Declare a variable named guess
 * Initialise guess to 0
 * While the user’s guess is not correct (i.e. while guess != goal ):
 * Ask the user to enter their guess]
 * Store the guess in the guess variable
 * If the guess is greater than the goal , print “Too high, try again”
 * Else if the guess is less than the goal , print “Too low, try again”
 * Else print the message “Perfect!!”
 * Print “Goodbye”
 */
public class GuessingGame {

    private int printPromptGetIntResponse(String prompt_msg) {
        System.out.println(prompt_msg);
        String input_str = Keyboard.readInput();
        return Integer.parseInt(input_str);
    }

    public void start() {

        // TODO Write your code here.
        int goal = (int) (Math.random() * 100);
        int guess = 0;

        while (guess != goal) {
            guess = printPromptGetIntResponse("Enter your guess (1 – 100)");
           // helper while debugging its good to know the random early
            System.out.println("the random number is " + goal);

            if (guess < goal) {
                System.out.println("Too low, try again");

            } else if (guess > goal) {
                System.out.println("Too high, try again");
            }
        }
        // you must be right
        System.out.println("Right on - the random number is: " + goal + " your guess was: " + guess);
        System.out.println("Perfect!");

        // while(guess != goal){
        // Ask for a number
        // guess = userInput
        // String input = Keyboard.readInput();
        // if guess < goal
        // System.out.println("too low silly");
        // else if guess > goal
        // System.out.println("too high silly");
        // else print "perfect"


    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
